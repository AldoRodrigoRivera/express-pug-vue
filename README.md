# VUE + PUG + EXPRESS.JS #

### JQUERY ###
  script(src='https://code.jquery.com/jquery-3.2.1.min.js', integrity='sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=', crossorigin='anonymous')

### VUE + AXIOS ###
  script(src="https://unpkg.com/axios/dist/axios.min.js")
  script(src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.4.2/vue.js")

### TOASTR ###
  script(src='https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js")
  script(src='https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css")

### Contribution guidelines ###
https://vuejs.org/v2/guide/events.html
https://vuejs.org/v2/guide/forms.html#v-model-with-Components
https://booker.codes/input-validation-in-express-with-express-validator/
https://github.com/CodeSeven/toastr